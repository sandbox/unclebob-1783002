<?php

/**
 * @file
 * An interoperability extension for external authentication.
 *
 * Semi-anonymous user creates real, temporary users for Drupal but
 * does not leave any data in Drupal's {user} table. Instead it
 * keeps data only in $_SESSION and implements hook_user().
 * You can use this module's functions to present profile
 * and custom personal information which does not reside
 * in Drupal's database securely to the Drupal user
 * as if it did.
 */


/**
 * Display help and module information.
 *
 * @param string $path
 *   which path of the site we're displaying help
 * @param array $arg
 *   array that holds the current path as would be returned
 *   from arg() function
 *
 * @return string
 *   help text for the path
 */
function semianonuser_help($path, $arg) {

  switch ($path) {
    case "admin/help#semianonuser":
      $output = '<p>' . t("Extends external authentication by creating a Drupal user. This user will be given profile information like any Drupal user may have, but does not maintain credential or profile information after semianonuser_logout, instead it is decoupled from the session having never been stored in {user} but instead just php's session. You can use this module's functions to present profile and custom personal information which does not reside in Drupal's database securely to the Drupal user as if it did. Intended to hold for a current session only profile information of users authenticated and administrated on an external resource. A semi-anonoymous drupal user is useful to enable your secure module's interoperability with Drupal and other contrib modules which expect Drupal to have users in it's own database and behaving according to the Drupal norms. Please note http://drupal.org/node/219100 and declare dependancies if developing modules which will use this module. http://api.drupal.org/api/drupal/developer--globals.php/global/user/6 may also be of use.") . '</p>';
      break;
  }
  return $output;
}


/**
 * Creates a string of digits randomly.
 *
 * @param int $many
 *   an integer specifying the length required
 *
 * @return string
 *   a string containing a number of digits.
 */
function _semianonuser_digits($many) {
  $outstring = '';
  for ($n = 1; $n <= $many; $n++) {
    $outstring .= rand(0, 9);
  }
  return $outstring;
}


/**
 * Logs in a newly created seminanonymous user and records user data against it.
 *
 * @param array $your_user
 *   an associative array of user data
 */
function semianonuser_do_login($your_user) {
  global $user;
  unset($_SESSION['semianonuser']);

  // Create a good semianonuser account name for this user.
  $try = 'semianonuser_user_' . session_id() . '_session_named';
  while (user_load(array('name' => $try))) {
    $try = 'semianonuser_user' . session_id() . '_' . _semianonuser_digits(4) . '_renamed';
  }

  // Create and authenticate the seminanonusername [modifies $user].
  user_external_login_register($try, 'semianonuser');

  // Update user details and the seminanon session for your_user.
  $_SESSION['semianonuser']['loggedin'] = 'true';
  semianonuser_do_update($your_user);
  return;
}


/**
 * Updates the semianonymous user data.
 *
 * @param array $your_user
 *   an associative array of user data
 */
function semianonuser_do_update($your_user) {
  global $user;
  $data = unserialize($user->data);
  $known_user_keys = array('name',
    'mail',
    'mode',
    'sort',
    'threshold',
    'theme',
    'signature',
    'signature_format',
    'status',
    'timezone',
    'language',
    'picture',
    'init',
    'timezone_name',
    'roles',
    'nodewords');
  // Not modifiable, would end up in serialized data along with the unknowns;
  // 'uid', 'pass', 'created', 'access' & 'login'.
  foreach ($your_user as $detail => $value) {
    $_SESSION['semianonuser'][$detail] = $value;
    if (in_array($detail, $known_user_keys)) {
      $user->$detail = $value;
    }
    else {
      $data[$detail] = $value;
    }
  }
  $user->data = serialize($data);
}


/**
 * Logs the curent semianonymous out.
 */
function semianonuser_do_logout() {
  unset($_SESSION['semianonuser']);
}


/**
 * Implements hook_user().
 */
function semianonuser_user($op, &$edit, &$account, $category = NULL) {

  if ($op == 'load') {
    if ($_SESSION['semianonuser']['loggedin'] == 'true') {
      $data = unserialize($account->data);
      $known_user_keys = array('name',
        'mail',
        'mode',
        'sort',
        'threshold',
        'theme',
        'signature',
        'signature_format',
        'status',
        'timezone',
        'language',
        'picture',
        'init',
        'timezone_name',
        'roles',
        'nodewords');
      foreach ($_SESSION['semianonuser'] as $detail => $value) {
        if ($detail != 'loggedin') {
          if (in_array($detail, $known_user_keys)) {
            $account->$detail = $value;
          }
          else {
            $data[$detail] = $value;
          }
        }
      }
      $user->data = serialize($data);
    }
  }
}


/**
 * Implements hook_cron().
 */
function semianonuser_cron() {
  $time = time() - 129600;
  $namestring = "semianonuser_user_";
  $endstring = "_named";
  db_query('DELETE FROM {users} WHERE access < %d AND name LIKE "%s%%%s%%"', $time, '%' . $namestring . '%', '%' . $endstring);
}
