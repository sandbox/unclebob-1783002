README.txt
==========


Semi-anonymous user is an interoperability extension for external
authentication. Data you wish to store remotely from the Drupal database about
users in an external authentication configuration is made available and usable
for other modules without needing to be recorded in the users table.

Semi-anonymous user creates real, temporary users in Drupal for Drupal to
associate with your external data but it does not give those users any of your
external data. $_SESSION and {session} only are used, not {users} and the data
persists only as long as your php session ID. Then the semi-anonymous user
implements hook_user() to retrieve it's session data, making it available in the
global user for other modules to use.

You can use this module's functions to present profile and custom, personal
information which can not, or should not reside in Drupal's database safely to
the Drupal user as if it did.


Brief History
=============

Unless a user logs in through the Drupal login they do not have their personal
data carried by the global user object. And if your users need to be
authenticated against a remote device, system, or API then they may never
actually log in through the Drupal login. That could mean a module requiring or
offering email communication to your users, or any other functionality dependant
on information usualy stored in a profile could fail. Semianonuser allows you to
fill the gap from your data about users smoothly and simply to the global user.
It is an ideal way to handle profile information deemed too sensitive to be
stored in the Drupal database.


Functionality
=============

Once your module has gathered it's user data, you should call
semianonuser_do_login and pass an associative array.

semianonuser_do_login(array('detail' => 'thing about user',
'email' => 'email address'));

Anything passed in will appear in the global user array to all other modules,
so pass in anything you wish to see available.

When you are logging out your user or destrying their session call
semianonuser_do_logout().

Your data will not be entered into the user table, instead a randomly generated
username will be created and entered into the user table with a bare minimum of
detail instead. Your data is stored in the php session and merged onto this
random user to create the global Drupal user letting all Drupal modules see the
details you're hiding.

If a logout occurs, or the session's expiry time elapses this sesion table row
is deleted. Cron will clean out all semi-anonymous users 24 hours after their
last access. Hook_unistall is implemented to clean up after uninstallation.
